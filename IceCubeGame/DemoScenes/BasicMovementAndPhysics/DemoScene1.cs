﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Monogame/XNA:
using Microsoft.Xna.Framework;

// Aurora
using Aurora.core.components;
using Aurora.core.objects.display;


// SceneObjects
using Aurora.IceCubeGame.Objects;

namespace Aurora.IceCubeGame.DemoScenes.BasicMovementAndPhysics
{
    class DemoScene1 : Scene
    {
        public DemoScene1() : base("IceCubeDemoScene1") {

        }

        /// <summary>
        /// Loads in all content for this Scene:
        /// </summary>
        public override void Load() {
            base.Load(new string[] { "SnowyPlatformer/ice_cube", "SnowyPlatformer/angry_furnace", "SnowyPlatformer/snowflake", "SnowyPlatformer/Floor_Wide" });
        }

        /// <summary>
        ///  Creates the Scene objects:
        /// </summary>
        public override void CreateScene() {
            // Adds terrain to move around on:
            Sprite floor = AddChild(new Sprite("SnowyPlatformer/Floor_Wide"));
            floor.Position.X = 300;
            floor.Position.Y = 750;
            floor.Physics.Enabled = true;
            floor.Physics.IsCollidable = true;
            floor.Physics.IsSolid = true;
            floor.Physics.IsFixed = true;
            floor.UpdateTransform();
            floor.Physics.Mass = 0;

            Sprite floor2 = AddChild(new Sprite("SnowyPlatformer/Floor_Wide"));
            floor2.Position.X = 700;
            floor2.Position.Y = 350;
            floor2.Physics.Enabled = true;
            floor2.Physics.IsCollidable = true;
            floor2.Physics.IsSolid = true;
            floor2.Physics.IsFixed = true;
            floor2.UpdateTransform();
            floor2.Physics.Mass = 0;

            // Add the hero:
            IceCubeGame.Objects.Characters.IceCubeHero Hero = AddChild(new Objects.Characters.IceCubeHero());
            Hero.Position.Y = 400;
            Hero.Position.X = 1920/2;
            Hero.UpdateTransform();
            Hero.Physics.Mass = 2;

            Objects.Items.Collectable Snowflake = AddChild(new Objects.Items.Collectable());
            Snowflake.Position = new Vector2(800, 700);

            // Add a  thing to play with:
            float[] xPositions = { 100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600 };


            for ( int i = 0; i < xPositions.Length; i++) {
                Sprite FurnaceToy = AddChild(new Sprite("SnowyPlatformer/angry_furnace"));
                FurnaceToy.Physics.Enabled = true;
                FurnaceToy.Physics.IsCollidable = true;
                FurnaceToy.Physics.IsSolid = true;
                FurnaceToy.Position.Y = 0;
                FurnaceToy.Position.X = xPositions[i];
                FurnaceToy.Physics.Acceleration.Y = 0.2f;
            }
        }

        public override void Update(GameTime gT) {
            base.Update(gT);
        }
    }
}
