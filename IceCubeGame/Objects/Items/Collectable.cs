﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Microsoft/XNA:
using Microsoft.Xna.Framework;

// Aurora:
using Aurora.core.objects.main;
using Aurora.core.objects.display;

namespace Aurora.IceCubeGame.Objects.Items
{
    class Collectable : Sprite
    {
        public Collectable() : base("SnowyPlatformer/snowflake")
        {
            this.Physics.Enabled = true;
            this.Physics.IsCollidable = true;
        }

        public override void OnCollision(GameObject other)
        {
            if ( other.Name == "IceCubeHero")
            {
                Position = new Vector2(-200, -200);
            } else
            {
                this.Physics.Acceleration.Y = 0;
            }
            base.OnCollision(other);
        }
    }
}
