﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Monogame/Xna:
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

// Aurora:
using Aurora.core.objects.display;

namespace Aurora.IceCubeGame.Objects.Characters
{
    class IceCubeHero : Sprite
    {
        /// <summary>
        /// Creates the IceCubeHero:
        /// </summary>
        public IceCubeHero() : base("SnowyPlatformer/ice_cube") {
            Name = "IceCubeHero";

            Physics.Enabled = true;
            Physics.IsCollidable = true;
            Physics.IsSolid = true;

            Physics.Acceleration.Y = 0.4f;

            this.AddChild
        }

        /// <summary>
        /// IceCubeHero updates!
        /// </summary>
        /// <param name="gt"></param>
        public override void Update(GameTime gt) {
            // Acceleration is 0 - gets changed based on user inputs:
            Physics.Acceleration.X = 0;
            Physics.Acceleration.Y = 0.1f;

            // Apply acceleration based off user inputs:
            KeyboardState kbs = Keyboard.GetState();

            if ( kbs.IsKeyDown(Keys.A)) {
                Physics.Acceleration.X = -0.1f;
            }
            if ( kbs.IsKeyDown(Keys.D)) {
                Physics.Acceleration.X = 0.1f;
            }

            if (kbs.IsKeyDown(Keys.Space)) {
                Physics.Acceleration.Y = -0.2f;
            }

            // Base object updates:
            base.Update(gt);
        }
    }
}
