﻿namespace Aurora
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main() {
            using (var game = new Main())
                game.Run();
        }
    }
}