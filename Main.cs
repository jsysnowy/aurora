﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Aurora
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {
        private bool usingSpine = false;
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private Spine.SkeletonRenderer spineSkeletonRenderer;
        private Spine.Skeleton knightSkeleton;
        private Spine.Atlas knightAtlas;
        private Spine.AnimationState knightState;

        public Main() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = (int)core.Aurora.DIMENTIONS.X;
            graphics.PreferredBackBufferHeight = (int)core.Aurora.DIMENTIONS.Y;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            if (usingSpine) {
                // Create spine renderer:
                spineSkeletonRenderer = new Spine.SkeletonRenderer(GraphicsDevice);
                spineSkeletonRenderer.PremultipliedAlpha = false;

                // Create Spine:

                knightSkeleton.X = GraphicsDevice.Viewport.Width / 2;
                knightSkeleton.Y = GraphicsDevice.Viewport.Height / 2;

                //knightSkeleton.SetSkin(knightSkeletonData.skins.Items[2]);
                knightState.SetAnimation(0, "Idle", true);
            }

            // Core XNA setup:
            base.Initialize();

            // Initialise GameFramework.
            core.Aurora.Init(Content, GraphicsDevice);

            // Initialise TestScene:
            core.Aurora.SceneManager.Add( new IceCubeGame.DemoScenes.BasicMovementAndPhysics.DemoScene1(), true);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            if (usingSpine) {
                knightState.Update(gameTime.ElapsedGameTime.Milliseconds / 1000.0f);
                knightState.Apply(knightSkeleton);
                knightSkeleton.UpdateWorldTransform();
            }

            core.Aurora.SceneManager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Draw spine:
            if (usingSpine) {
                ((BasicEffect)spineSkeletonRenderer.Effect).Projection = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 1, 0);
                spineSkeletonRenderer.Begin();
                spineSkeletonRenderer.Draw(knightSkeleton);
                spineSkeletonRenderer.End();
            }

            core.Aurora.SceneManager.Draw();

            base.Draw(gameTime);
        }
    }
}