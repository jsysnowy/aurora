﻿// Aurora:
using Aurora.core.components;

// Microsoft/XNA
using Microsoft.Xna.Framework;
using System.Collections.Generic;

/// <summary>
/// SceneManager handles all scenes - init, loading, unloading, updating, rendering etc.
/// </summary>

namespace Aurora.core.managers
{
    internal class SceneManager
    {
        /// <summary>
        /// Stores all current scenes:
        /// // keys true = active, false = inactive
        /// </summary>
        private Dictionary<bool, List<Scene>> _currentScenes;

        /// <summary>
        /// Creates new SceneManager:
        /// </summary>
        public SceneManager() { }

        /// <summary>
        /// Initialises the SceneManager;
        /// </summary>
        public void Init() {
            // Create Dictionary storing scenes:
            _currentScenes = new Dictionary<bool, List<Scene>>();
            _currentScenes[true] = new List<Scene>();
            _currentScenes[false] = new List<Scene>();
        }

        /// <summary>
        /// Adds a Scene to the SceneManager:
        /// </summary>
        /// <param name="sceneToAdd"></param>
        /// <param name="andEnable"></param>
        public void Add<T>(T sceneToAdd, bool andEnable = false) where T : Scene {
            _currentScenes[andEnable].Add(sceneToAdd);
            if (andEnable) {
                Enable(sceneToAdd);
            }
        }

        /// <summary>
        /// Enables passed in scene:
        /// </summary>
        /// <param name="sceneToEnable"></param>
        public void Enable<T>(T sceneToEnable) where T : Scene {
            sceneToEnable.Init();
            sceneToEnable.Load();
        }

        /// <summary>
        /// Updates the scene:
        /// </summary>
        /// <param name="gT"></param>
        public virtual void Update(GameTime gT) {
            // Updates all currentScenes which are active:
            _currentScenes[true]?.ForEach(s => {
                s.Update(gT);
            });
        }

        /// <summary>
        /// Draws the scene:
        /// </summary>
        public virtual void Draw() {
            // Draws all currentScenes which are active:
            _currentScenes[true]?.ForEach(s => {
                s.Draw();
            });
        }
    }
}