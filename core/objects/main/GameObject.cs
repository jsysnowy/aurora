﻿// Aurora:
using Aurora.core.plugins.main;

// Monogame/XNA:
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Aurora.core.objects.main
{
    internal class GameObject
    {
        /// <summary>
        /// Stores if this GameObject is currently active nor not.
        /// Active being false will make this do nothing. No updates, no rendering.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Alive will start/stop this GameObject updating but it will still render.
        /// </summary>
        public bool Alive { get; set; }

        /// <summary>
        /// Visible will start/stop this GameObject rendering but it will still update.
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Stores optional Name property of this GameObject:
        /// </summary>
        public string Name;

        /// <summary>
        /// Stores local position of this GameObject:
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Stores local scale of this GameObject:
        /// </summary>
        public float Scale;

        /// <summary>
        /// Stores local rotation of this GameObject:
        /// </summary>
        public float Rotation;

        /// <summary>
        /// Stores Anchor of this object:
        /// </summary>
        public Vector2 Anchor;

        /// <summary>
        /// Stores WorldTransform of the GameObject;
        /// </summary>
        public modules.GameObjectWorldTransform WorldTransform;

        /// <summary>
        /// Stores bounds of a GameObject:
        /// </summary>
        public modules.GameObjectBounds Bounds;

        /// <summary>
        /// Physics currently applied to this GameObject:
        /// </summary>
        public modules.GameObjectPhysics Physics;

        /// <summary>
        /// Scene this GameObject belongs to:
        /// </summary>
        public components.Scene Scene { get; protected set; }

        /// <summary>
        /// Stores parent of this GameObject:
        /// </summary>
        public GameObject Parent { get; private set; }

        /// <summary>
        /// Stores all children which are a part of this GameObject:
        /// </summary>
        public List<GameObject> Children { get; private set; }

        /// <summary>
        /// Dictionary which stores all plugins attached to this GameObject
        /// </summary>
        private List<Plugin> Plugins;

        /// <summary>
        /// Creates base GameObject.
        /// </summary>
        public GameObject() {
            // Default values:
            Active = true;
            Alive = true;
            Visible = true;
            Position = new Vector2(0, 0);
            Scale = 1.0f;
            Rotation = 0;
            Anchor = new Vector2(0, 0);
            Plugins = new List<Plugin>();

            WorldTransform = new modules.GameObjectWorldTransform();
            Bounds = new modules.GameObjectBounds();
            Physics = new modules.GameObjectPhysics();

            // Create list of possible children:
            Children = new List<GameObject>();
        }

        /// <summary>
        /// Adds child to this GameObject:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="childToAdd"></param>
        public T AddChild<T>(T childToAdd) where T : GameObject {
            // Remove from old Parent:
            if (childToAdd.Parent != null) {
                childToAdd.RemoveChild(childToAdd);
            }

            // Set new parent:
            childToAdd.Parent = this;
            childToAdd.Scene = this.Scene;
            this.Scene.AddSceneObject(childToAdd);

            // Add to children list:
            if (Children.IndexOf(childToAdd) == -1) {
                Children.Add(childToAdd);
            }

            // Return child:
            return childToAdd;
        }

        /// <summary>
        /// Removes child from this GameObject:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="childToRemove"></param>
        public void RemoveChild<T>(T childToRemove) where T : GameObject {
            Children.RemoveAt(Children.IndexOf(childToRemove));
            this.Scene.RemoveSceneObject(childToRemove);
        }

        #region ModuleManagement.

        /// <summary>
        /// Add a module to a RenderableObject
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T AddModule<T>() where T : Plugin, new() {
            // Made our Module List if it doesn't exist:
            // Make sure module can only be added once.
            for (int i = 0; i < Plugins.Count; i++) {
                if (typeof(T).IsInstanceOfType(Plugins[i])) {
                    return (T)Plugins[i];
                }
            }

            T newModule = new T {
                Object = this
            };

            Plugins.Add(newModule);
            return newModule;
        }

        /// <summary>
        /// Get and return module with type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetModule<T>() where T : Plugin {
            // Find and return our module.
            for (int i = 0; i < Plugins.Count; i++) {
                if (typeof(T).IsInstanceOfType(Plugins[i])) {
                    return (T)Plugins[i];
                }
            }

            // No module was found.
            return null;
        }

        /// <summary>
        /// Find and remove module with type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool RemoveModule<T>() where T : Plugin {
            // Find and remove our module.
            for (int i = 0; i < Plugins.Count; i++) {
                if (typeof(T).IsInstanceOfType(Plugins[i])) {
                    Plugins.RemoveAt(i);
                    return true;
                }
            }

            // No module was removed.
            return false;
        }

        #endregion ModuleManagement.

        /// <summary>
        /// Called every frame - this will allow user to Update this gameobject - changing params on a per-frame basis:
        /// </summary>
        /// <param name="gt"></param>
        public virtual void Update(GameTime gt) {
            // All plugins in this GameObject update:
            Plugins.ForEach(plugin => {
                plugin.Update(gt);
            });

            // Update all children:
            int index = 0;
            while (index != Children.Count) {
                Children[index].Update(gt);
                index++;
            }

            // Updates physics if enabled:
            if ( Physics.Enabled) {
                Physics.Update();
            }
        }

        /// <summary>
        /// Applies this gameobjects config:
        /// </summary>
        public void Apply() {
            // Updates physics if required:

            // Check for collisions:
        }

        /// <summary>
        /// Called when this GameObject is collided with another game object
        /// - this required a collision plugin to be triggered.
        /// </summary>
        /// <param name="other"></param>
        public virtual void OnCollision(GameObject other) {
        }

        /// <summary>
        /// Updates world transform on this GameObject:
        /// </summary>
        public void UpdateTransform() {
            WorldTransform.Position = Parent.WorldTransform.Position;
            WorldTransform.Rotation = Parent.WorldTransform.Rotation;
            WorldTransform.Scale = Parent.WorldTransform.Scale;

            Vector2 modPosition = new Vector2(
                (float)(Position.X * Math.Cos(WorldTransform.Rotation) - Position.Y * Math.Sin(WorldTransform.Rotation)),
                (float)(Position.Y * Math.Cos(WorldTransform.Rotation) + Position.X * Math.Sin(WorldTransform.Rotation))
            );

            modPosition.X *= WorldTransform.Scale;
            modPosition.Y *= WorldTransform.Scale;
            WorldTransform.Position += modPosition;
            WorldTransform.Scale *= Scale;
            WorldTransform.Rotation += Rotation;
            Bounds.Rect.Y = (int)WorldTransform.Position.Y;
            Bounds.Rect.X = (int)WorldTransform.Position.X;
        }

        /// <summary>
        /// Draws this object:
        /// </summary>
        public virtual void Draw() {
            int index = 0;
            while (index != Children.Count) {
                Children[index].UpdateTransform();
                Children[index].Draw();
                index++;
            }
        }
    }
}