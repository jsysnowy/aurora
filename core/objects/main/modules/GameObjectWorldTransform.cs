﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Monogame/XNA:
using Microsoft.Xna.Framework;

namespace Aurora.core.objects.main.modules
{
    class GameObjectWorldTransform
    {
        /// <summary>
        /// Stores world position:
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Stores world rotation:
        /// </summary>
        public float Rotation;

        /// <summary>
        /// Stores scale:
        /// </summary>
        public float Scale;

        /// <summary>
        /// Creates an instance of GameObjectWorldTransform:
        /// </summary>
        public GameObjectWorldTransform() {
            Position = new Vector2(0, 0);
            Rotation = 0;
            Scale = 1;
        }
    }
}
