﻿// Monogame/XNA:
using Microsoft.Xna.Framework;

namespace Aurora.core.objects.main.modules
{
    internal class GameObjectBounds
    {
        /// <summary>
        /// Stores rectangle hitbox for AABB collision:
        /// </summary>
        public Rectangle Rect;

        /// <summary>
        /// Inits Bounds with empty rect:
        /// </summary>
        public GameObjectBounds() {
            Rect = new Rectangle(0, 0, 0, 0);
        }

        /// <summary>
        /// Projects a translation onto current rect:
        /// </summary>
        /// <param name="translationIn"></param>
        /// <returns></returns>
        public Rectangle Project(Vector2 translationIn) {
            return new Rectangle(
                Rect.X + (int)translationIn.X,
                Rect.Y + (int)translationIn.Y,
                Rect.Width,
                Rect.Height
            );
        }
    }
}