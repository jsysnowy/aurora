﻿// Microsoft/XNA:
using Microsoft.Xna.Framework;

namespace Aurora.core.objects.main.modules
{
    internal class GameObjectPhysics
    {
        /// <summary>
        /// Stores if physics is enabled or not:
        /// </summary>
        public bool Enabled;

        /// <summary>
        /// Stores if this object can collide;
        /// </summary>
        public bool IsCollidable;

        /// <summary>
        /// Stores if this object is solid;
        /// </summary>
        public bool IsSolid;

        /// <summary>
        /// Stores if this object is fixed in place:
        /// </summary>
        public bool IsFixed;

        /// <summary>
        /// Stores velocity of this object:
        /// </summary>
        public Vector2 Velocity;

        /// <summary>
        /// Stores acceleration of this object:
        /// </summary>
        public Vector2 Acceleration;

        /// <summary>
        /// Stores maximum possible allowed speed:
        /// </summary>
        public float MaxSpeed;

        /// <summary>
        /// Stores friction modifier;
        /// </summary>
        public float Friction;

        /// <summary>
        /// Stores restitution (bounciness) of object:
        /// </summary>
        public float Restitution;

        /// <summary>
        /// Stores mass of object:
        /// </summary>
        public float Mass;

        /// <summary>
        /// Creates new instance of GameObjectPhysics with default values:
        /// </summary>
        public GameObjectPhysics() {
            Velocity = new Vector2(0, 0);
            Acceleration = new Vector2(0, 0);
            MaxSpeed = 25;
            Friction = 0.99f;
            Restitution = 0.2f;
            Mass = 1;
            IsCollidable = false;
            IsSolid = false;
        }

        /// <summary>
        /// Updates this physics module:
        /// </summary>
        public void Update() {
            this.Velocity.X *= Friction;
            this.Velocity.X += this.Acceleration.X;
            this.Velocity.Y += this.Acceleration.Y;
        }
    }
}