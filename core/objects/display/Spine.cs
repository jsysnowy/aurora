﻿// Spine runtimes:
// Aurora:
using Aurora.core.objects.main;
using Microsoft.Xna.Framework;
using Spine;

namespace Aurora.core.objects.display
{
    internal class Spine : GameObject
    {
        public string SpineID { get; protected set; }
        private Skeleton _skeleton;
        private Atlas _atlas;
        private AnimationState _animState;

        public Skeleton Skeleton {
            get {
                return _skeleton;
            }
        }

        /// <summary>
        /// Creates new Spine Gameobject:
        /// </summary>
        /// <param name="spineID_"></param>
        public Spine(string spineID_) {
            this.SpineID = spineID_;
            _generateSpine();
        }

        /// <summary>
        /// Generates spine:
        /// </summary>
        private void _generateSpine() {
            _atlas = new Atlas("Content/" + SpineID + ".atlas", new XnaTextureLoader(Aurora.GD));

            SkeletonJson skeletonJson = new SkeletonJson(_atlas);
            SkeletonData skeletonData = skeletonJson.ReadSkeletonData("Content/" + SpineID + ".json");

            _skeleton = new Skeleton(skeletonData);
            AnimationStateData knightAnimationStateData = new AnimationStateData(_skeleton.Data);
            _animState = new AnimationState(knightAnimationStateData);
            _skeleton.X = 0;
            _skeleton.Y = 0;
        }

        /// <summary>
        /// Updates this GameObject:
        /// </summary>
        /// <param name="gt"></param>
        public override void Update(GameTime gt) {
            _animState.Update(gt.ElapsedGameTime.Milliseconds / 1000.0f);
            _animState.Apply(_skeleton);
            _skeleton.UpdateWorldTransform();
            base.Update(gt);
        }

        /// <summary>
        /// Draws this GameObject:
        /// </summary>
        public override void Draw() {
            System.Diagnostics.Trace.WriteLine("?");
            Scene.SpineRenderer.Draw(_skeleton);
            base.Draw();
        }
    }
}