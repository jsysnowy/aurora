﻿// Monogame/XNA:
// Aurora:
using Aurora.core.objects.main;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Aurora.core.objects.display
{
    internal class Sprite : GameObject
    {
        /// <summary>
        /// Stores TextureID of this Sprite:
        /// </summary>
        public string TextureID;

        /// <summary>
        /// Stores Texture applied to this sprite:
        /// </summary>
        public Texture2D Texture { get; private set; }

        /// <summary>
        /// Creates new sprite with passed in texture:
        /// </summary>
        /// <param name="textureID"></param>
        public Sprite(string textureID) : base() {
            TextureID = textureID;
        }

        /// <summary>
        /// Updates:
        /// </summary>
        /// <param name="gt"></param>
        public override void Update(GameTime gt) {
            // Sets texture:
            Texture = this.Scene.TextureDict[TextureID];

            // Sets bounds to texture:
            Bounds.Rect.Width = Texture.Width;
            Bounds.Rect.Height = Texture.Height;

            // Updates base GO:
            base.Update(gt);
        }

        /// <summary>
        /// Draws this sprite:
        /// </summary>
        /// <param name="worldPositionMatrix"></param>
        public override void Draw() {
            Scene.SpriteBatch.Draw(Texture, WorldTransform.Position, null, Color.White, WorldTransform.Rotation, new Vector2(Texture.Width * Anchor.X, Texture.Height * Anchor.Y), WorldTransform.Scale, SpriteEffects.None, 0);
            base.Draw();

            //CollisionAABB collisionModule = GetModule<CollisionAABB>();
            //if (collisionModule != null) {
            //    Scene.SpriteBatch.DrawRectangle(collisionModule.Bounds, Color.White, 4);
            //}
        }
    }
}