﻿// Aurora:
using Aurora.core.objects.main;

// MonogameXNA:
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// Spine Runtimes:
using Spine;
using System.Collections.Generic;

/// <summary>
/// Scenes contain a single segment of a game, and are handled by SceneManager.
/// </summary>
namespace Aurora.core.components
{
    internal class Scene : GameObject
    {
        /// <summary>
        /// Renders basic textures.
        /// </summary>
        public SpriteBatch SpriteBatch { get; private set; }

        /// <summary>
        /// Stores spine renderer:
        /// </summary>
        public SkeletonRenderer SpineRenderer;

        /// <summary>
        /// Stores TextureDictionary this Scene has access to:
        /// </summary>
        public Dictionary<string, Texture2D> TextureDict { get; private set; }

        /// <summary>
        /// Stores active camera on the scene:
        /// </summary>
        public Camera ActiveCamera { get; private set; }

        /// <summary>
        ///  Stores the Collision System for this scene:
        /// </summary>
        public systems.CollisionSystem CollisionSystem;

        /// <summary>
        /// List of all GameObjects associated to this scene:
        /// </summary>
        private List<GameObject> _allSceneObjectsRef;

        /// <summary>
        /// Creates new scene with passed in name:
        /// </summary>
        /// <param name="name"></param>
        public Scene(string name) {
            this.Name = name;
            this.Scene = this;
        }

        /// <summary>
        /// Initialises scene, ready to be used.
        /// </summary>
        public virtual void Init() {
            SpriteBatch = new SpriteBatch(core.Aurora.GD);
            TextureDict = new Dictionary<string, Texture2D>();
            SpineRenderer = new SkeletonRenderer(core.Aurora.GD);
            SpineRenderer.PremultipliedAlpha = false;

            ActiveCamera = new Camera(Aurora.GD.Viewport);
            ActiveCamera.X = Aurora.GD.Viewport.Width / 2;
            ActiveCamera.Y = Aurora.GD.Viewport.Height / 2;

            CollisionSystem = new systems.CollisionSystem();
            _allSceneObjectsRef = new List<GameObject>();
        }

        /// <summary>
        /// Creates the Scene:
        /// </summary>
        public virtual void CreateScene() {
            // To be overwritten:
        }

        /// <summary>
        /// Scene can be created without loading anything in:
        /// </summary>
        public virtual void Load() { }

        /// <summary>
        /// Loads in all textures required for this scene:
        /// </summary>
        /// <param name="LoadedTextureIDs"></param>
        public virtual void Load(string[] LoadedTextureIDs) {
            // Loop over and load all TexIDs passed in:
            foreach (string TexID in LoadedTextureIDs) {
                TextureDict.Add(TexID, Aurora.Content.Load<Texture2D>(TexID));
            }

            CreateScene();
        }

        /// <summary>
        /// Unloads in textures:
        /// </summary>
        public virtual void Unload() {
        }

        /// <summary>
        /// Adds reference to every single gameobject associated to this scene:
        /// </summary>
        /// <param name="goIn"></param>
        public void AddSceneObject(GameObject goIn) {
            if (_allSceneObjectsRef.IndexOf(goIn) == -1) {
                _allSceneObjectsRef.Add(goIn);
            }
        }

        /// <summary>
        /// Remove reference for gameobject to this scene
        /// </summary>
        /// <param name="goIn"></param>
        public void RemoveSceneObject(GameObject goIn) {
            _allSceneObjectsRef.Remove(goIn);
        }

        /// <summary>
        /// Updates the scene:
        /// </summary>
        /// <param name="gT"></param>
        public override void Update(GameTime gT) {
            // Camera updates:
            ActiveCamera.Update();

            // All gameobjects on this scene update:
            base.Update(gT);

            // Collision system prepares itself:
            CollisionSystem.Clear();

            // All collidable objects prepare themself:
            _allSceneObjectsRef.ForEach(sceneObject => {
                if ( sceneObject.Physics.Enabled && sceneObject.Physics.IsCollidable) {
                    CollisionSystem.AddCollisionObject(sceneObject);
                }
            });

            // Check for collisions:
            CollisionSystem.Update();

            // All scene objects apply there physics if required:
            _allSceneObjectsRef.ForEach(sceneObject => {
                if (sceneObject.Physics.Enabled) {
                    sceneObject.Position += sceneObject.Physics.Velocity;
                }
            });

        }

        /// <summary>
        /// Draws the scene:
        /// </summary>
        public override void Draw() {
            SpriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, ActiveCamera.Transform);

            ((BasicEffect)SpineRenderer.Effect).Projection = Matrix.CreateOrthographicOffCenter(0, core.Aurora.GD.Viewport.Width, core.Aurora.GD.Viewport.Height, 0, 1, 0);
            SpineRenderer.Begin();
            base.Draw();
            SpineRenderer.End();
            SpriteBatch.End();
        }
    }
}