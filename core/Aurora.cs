﻿// Monogame/XNA:
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Aurora.core
{
    internal class Aurora
    {
        /// <summary>
        /// Stores the ContentManager for Aurora:
        /// </summary>
        public static ContentManager Content;

        /// <summary>
        /// Stores XNA GraphicsDevice used for rendering:
        /// </summary>
        public static GraphicsDevice GD { get; private set; }

        /// <summary>
        /// Stores the SceneManager which manages all scenes inside Aurora:
        /// </summary>
        public static managers.SceneManager SceneManager { get; private set; }

        /// <summary>
        /// Stores dimentions of game:
        /// </summary>
        public static Vector2 DIMENTIONS = new Vector2(2400, 1200);

        /// <summary>
        /// Inititalises Aurora - This is where all the magic starts :)
        /// </summary>
        /// <returns></returns>
        public static bool Init(ContentManager C, GraphicsDevice GD) {
            // Store reference to GraphicsDevice:
            Aurora.Content = C;
            Aurora.GD = GD;

            // Create the SceneManager:
            SceneManager = new managers.SceneManager();
            SceneManager.Init();

            // Return the initialise was successful:
            return true;
        }
    }
}