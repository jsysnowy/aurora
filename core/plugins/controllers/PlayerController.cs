﻿using Microsoft.Xna.Framework;

// Monogame/XNA:
using Microsoft.Xna.Framework.Input;

namespace Aurora.core.plugins.controllers
{
    /// <summary>
    /// Controller controlled by a player:
    /// </summary>
    internal class PlayerController : plugins.main.Plugin
    {
        public override void Update(GameTime gT) {
            // Get current keyboard state:
            KeyboardState KS = Keyboard.GetState();

            // Calc Acceleration:
            int accelX = 0;
            if (KS.IsKeyDown(Keys.A)) {
                accelX -= 1;
            }
            if (KS.IsKeyDown(Keys.D)) {
                accelX += 1;
            }

            int accelY = 0;
            if (KS.IsKeyDown(Keys.W)) {
                accelY -= 1;
            }
            if (KS.IsKeyDown(Keys.S)) {
                accelY += 1;
            }

            base.Update(gT);
        }
    }
}