﻿// Monogame/XNA
// Aurora:
using Aurora.core.objects.main;
using Microsoft.Xna.Framework;

namespace Aurora.core.plugins.main
{
    internal class Plugin
    {
        /// <summary>
        /// Stores Object this Plugin is attached to:
        /// </summary>
        public GameObject Object { get; set; }

        /// <summary>
        /// Creates new instance of Plugin:
        /// </summary>
        public Plugin() {
        }

        /// <summary>
        /// Updates this plugin:
        /// </summary>
        /// <param name="gT"></param>
        public virtual void Update(GameTime gT) {
        }
    }
}