﻿using System;

// Monogame/XNA:
using Microsoft.Xna.Framework;

// Aurora:
using Aurora.core.objects.main;

namespace Aurora.core.maths.collision
{
    internal class CollisionResolver
    {
        public static void ResolveCollision(GameObject go1, GameObject go2, Vector2 normal, float depth) {
            // Calculates the relative velocity:
            Vector2 relVelocity = go2.Physics.Velocity - go1.Physics.Velocity;

            // Store invese mass of objects, make inv-mass 0 if mass is 0 (infinite).
            float go1InvMass = 0;
            if ( go1.Physics.Mass != 0) {
                go1InvMass = 1 / go1.Physics.Mass;
            }
            float go2InvMass = 0;
            if (go2.Physics.Mass != 0) {
                go2InvMass = 1 / go2.Physics.Mass;
            }
            // Calculates the relative velocity based off the collision normal direction:
            float velocityAlongNormal = Vector2.Dot(relVelocity, normal);

            // Don't do anything if velocities are separating:
            if ( velocityAlongNormal > 0) {
                return;
            }

            // Calculates resitution of the collision:
            float e = Math.Min(go1.Physics.Restitution, go2.Physics.Restitution); // TODO change to bounciness?

            // Calculates the inpulse scalar:
            float j = -(1 + e) * velocityAlongNormal;
            j /= go1InvMass + go2InvMass;

            // Apply inpulse objects:
            Vector2 inpulse = j * normal;
            go1.Physics.Velocity -= go1InvMass * inpulse;
            go2.Physics.Velocity += go2InvMass * inpulse;

            // Correct posiitoning:
            float percent = 0.2f;
            float slop = 0.1f;
            Vector2 correction = (Math.Max(depth - slop, 0) / (go1InvMass + go2InvMass)) * percent * normal;
            go1.Position -= go1InvMass * correction;
            go2.Position += go2InvMass * correction;

        }

        public static void PositionalCorrection( GameObject go1, GameObject obj2, float penetration) {
        }
    }
}