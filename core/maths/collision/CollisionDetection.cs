﻿// Monogame/XNA:
using Microsoft.Xna.Framework;
using System;

// Aurora:

namespace Aurora.core.maths.collision
{
    internal class CollisionDetection
    {
        /// <summary>
        /// Detects a collision between two AABBs.
        /// </summary>
        /// <param name="rec1"></param>
        /// <param name="rec2"></param>
        /// <returns></returns>
        public static bool AABBvsAABB(Rectangle rec1, Rectangle rec2, out Vector2 normal, out float penetration) {
            // TODO: maybe pass ib object for its position cuz of anchoring..?

            // Define passed out normal and penetration of collision:
            normal = new Vector2(0, 0);
            penetration = 0;

            // Vector from A to B:
            Vector2 directionFromRect1ToRect2 = new Vector2(rec2.Center.X - rec1.Center.X, rec2.Center.Y - rec1.Center.Y);

            // HalfExtents for each object:
            float rec1ExtentX = rec1.Width / 2;
            float rec2ExtentX = rec2.Width / 2;

            // Calculate x axis overlap:
            float xOverlap = rec1ExtentX + rec2ExtentX - Math.Abs(directionFromRect1ToRect2.X);

            // SAT test on x axis:
            if (xOverlap > 0) {
                // HalfExtents on Y for each object:
                float rec1ExtentY = rec1.Height / 2;
                float rec2ExtentY = rec2.Height / 2;

                // Calculate y axis overlap:
                float yOverlap = rec1ExtentY + rec2ExtentY - Math.Abs(directionFromRect1ToRect2.Y);
                if (yOverlap > 0) {
                    
                    // Find out which axis is penetrating the least:
                    if (xOverlap < yOverlap) {
                        // X overlap is deeper:
                        if (directionFromRect1ToRect2.X < 0) {
                            normal = new Vector2(-1, 0);
                        } else {
                            normal = new Vector2(1, 0);
                        }

                        // Sets penetration depth to xOverlap:
                        penetration = xOverlap;
                        return true;
                    } else {
                        // Y overlap is deeper:
                        if (directionFromRect1ToRect2.Y < 0) {
                            normal = new Vector2(0, -1);
                        } else {
                            normal = new Vector2(0, 1);
                        }

                        // Sets penetration depth to xOverlap:
                        penetration = yOverlap;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}