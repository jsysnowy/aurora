﻿// Microsoft/XNA:
using Microsoft.Xna.Framework;

// Aurora:
using Aurora.core.objects.main;
using System.Collections.Generic;


namespace Aurora.core.systems
{
    internal class CollisionSystem
    {
        /// <summary>
        /// Stores list of all objects which can collide:
        /// </summary>
        private List<GameObject> CollisionObjects;

        /// <summary>
        /// Creates a new instance of CollisionManager:
        /// </summary>
        public CollisionSystem() {
            // Initialise the list
            CollisionObjects = new List<GameObject>();
        }

        /// <summary>
        /// Clears the CollisionSystem:
        /// </summary>
        public void Clear() {
            CollisionObjects = new List<GameObject>();
        }

        /// <summary>
        /// Adds a collision object to the CollisionManager:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="CollisionObject"></param>
        /// <returns></returns>
        public T AddCollisionObject<T>(T CollisionObject) where T : GameObject {
            // Add GameObject if it has a collision module:
                CollisionObjects.Add(CollisionObject);

            // Returns object back:
            return CollisionObject;
        }

        /// <summary>
        /// Updates this CollisionManager:
        /// </summary>
        public void Update() {
            SimpleCollisionTest();
        }

        /// <summary>
        /// Performs a simple collision test between objects in this CollisionManager:
        /// </summary>
        /// <returns></returns>
        private bool SimpleCollisionTest() {
            // Tests every object against every other object:
            foreach (GameObject obj1 in CollisionObjects) {
                foreach (GameObject obj2 in CollisionObjects) {
                    if (obj1 != obj2) {

                        // Stores vars for collisionnormal and penetration:
                        Vector2 CollisionNormal;
                        float Penetration;

                        if (maths.collision.CollisionDetection.AABBvsAABB(obj1.Bounds.Project(obj1.Physics.Velocity), obj2.Bounds.Rect, out CollisionNormal, out Penetration)) {
                            
                            // If obj2 was solid, then resolve collision between two objects:
                            if ( obj1.Physics.IsSolid && obj2.Physics.IsSolid) {
                                maths.collision.CollisionResolver.ResolveCollision(obj1, obj2, CollisionNormal, Penetration);
                            }

                            obj1.OnCollision(obj2);
                        }
                    }
                }
            }

            return true;
        }
    }
}