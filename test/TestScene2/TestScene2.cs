﻿// Microsoft/XNA:
// Aurora:
using Aurora.core.components;
using Microsoft.Xna.Framework;

namespace Aurora.test.TestScene2
{
    internal class TestScene2 : Scene
    {
        /// <summary>
        /// Stores the first rendered tile of this scene:
        /// </summary>
        private Scenery.Tile TheFirstTile;

        /// <summary>
        /// Stores the first rendered tile of this scene:
        /// </summary>
        private Scenery.Tile TheSecondTile;

        /// <summary>
        /// Constructor!
        /// </summary>
        public TestScene2() : base("TestScene2") {
        }

        /// <summary>
        /// Loads in all content for this Scene:
        /// </summary>
        public override void Load() {
            base.Load(new string[] { "ScifiShooter/Base_Tile" });
        }

        /// <summary>
        ///  Creates the Scene objects:
        /// </summary>
        public override void CreateScene() {
            // Add the first tile:
            TheFirstTile = new Scenery.Tile();
            TheFirstTile.Position.X = 1250;
            TheFirstTile.Position.Y = 200;
            TheFirstTile.Physics.IsFixed = true;
            AddChild(TheFirstTile);

            TheSecondTile = new Scenery.Tile();
            AddChild(TheSecondTile);
            TheSecondTile.Position.X = 1250;
            TheSecondTile.UpdateTransform();
            TheSecondTile.Physics.Acceleration.X = 0.2f;
            TheSecondTile.Physics.Acceleration.Y = 0.2f;

        }

        public override void Update(GameTime gT) {
            base.Update(gT);
        }
    }
}