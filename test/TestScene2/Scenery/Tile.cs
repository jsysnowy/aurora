﻿// Aurora:
using Aurora.core.objects.display;

namespace Aurora.test.TestScene2.Scenery
{
    internal class Tile : Sprite
    {
        /// <summary>
        ///  Creates a new BaseTile:
        /// </summary>
        public Tile() : base("ScifiShooter/Base_Tile") {
            Physics.Enabled = true;
            Physics.IsCollidable = true;
            Physics.IsSolid = true;
        }
    }
}