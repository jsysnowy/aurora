﻿// Monogame/XNA:
// Aurora:
using Aurora.core.components;
using Aurora.core.objects.display;
using Aurora.core.plugins.controllers;
using Microsoft.Xna.Framework;

namespace Aurora.test.TestScene
{
    internal class TestScene : Scene
    {
        /// <summary>
        /// Creates the test scene :)
        /// </summary>
        public TestScene() : base("TestScene") { }

        /// <summary>
        /// Stores the Earth:
        /// </summary>
        private Sprite Earth;

        /// <summary>
        /// Stores the moon:
        /// </summary>
        private Sprite Moon;

        /// <summary>
        /// Stores Text around earth.
        /// </summary>
        private Sprite HW;

        /// <summary>
        /// Stores Trampoline sprite:
        /// </summary>
        private Sprite Tramp;

        /// <summary>
        /// Loads in all sprites needed:
        /// </summary>
        public override void Load() {
            base.Load(new string[] { "SpaceScene/space_background", "SpaceScene/earth", "SpaceScene/moon", "SpaceScene/HelloWorld!", "SpaceScene/trampoline" });

            Sprite BG = this.AddChild(new Sprite("SpaceScene/space_background"));
            BG.Scale = 3;

            Earth = this.AddChild(new Sprite("SpaceScene/earth"));
            Earth.Position = new Vector2(core.Aurora.DIMENTIONS.X / 2, 100);
            Earth.Anchor = new Vector2(0.5f, 0.5f);
            Earth.Scale = 0.3f;
            Earth.UpdateTransform();

            HW = Earth.AddChild(new Sprite("SpaceScene/HelloWorld!"));
            HW.Position.Y = 0;
            HW.Anchor = new Vector2(0.5f, 0.5f);
            HW.Name = "HW";
            HW.Scale = 5.5f;

            Moon = Earth.AddChild(new Sprite("SpaceScene/moon"));
            Moon.Position.Y = 800;
            Moon.Anchor = new Vector2(0.5f, 0.5f);
            Moon.Name = "Moon";
            Moon.Scale = 1.5f;

            Sprite Moon2 = Moon.AddChild(new Sprite("SpaceScene/moon"));
            Moon2.Position.Y = 240;
            Moon2.Anchor = new Vector2(0.5f, 0.5f);
            Moon2.Name = "Moon2";
            Moon2.Scale = 0.8f;

            Tramp = this.AddChild(new Sprite("SpaceScene/trampoline"));
            Tramp.Position = new Vector2(core.Aurora.DIMENTIONS.X / 2 + 10, 1000);
            Tramp.Anchor = new Vector2(0.5f, 0.5f);
            Tramp.AddModule<PlayerController>();
            Tramp.UpdateTransform();
        }

        /// <summary>
        /// Space.
        /// </summary>
        /// <param name="gT"></param>
        public override void Update(GameTime gT) {
            Earth.Rotation += 0.1f;
            Moon.Rotation += 0.1f;
            base.Update(gT);
        }
    }
}